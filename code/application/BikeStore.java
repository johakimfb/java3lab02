// Johakim Fontaine Barboza 2038508
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Roland", 4, 100);
        bicycles[1] = new Bicycle("Activision", 30, 110);
        bicycles[2] = new Bicycle("Akai", 99, 200);
        bicycles[3] = new Bicycle("Ableton", 151515, 150);

        for (int i = 0; i < bicycles.length; i++) 
        {
            System.out.println(bicycles[i]);
        }
    }
}
