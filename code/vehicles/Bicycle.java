// Johakim Fontaine Barboza 2038508
package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    // Getters
    public String getManufacturer() {
        return this.manufacturer;
    }
    public int getNumberGears() {
        return this.numberGears;
    }
    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    // Constructer
    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    // toString
    public String toString() {
        return  "Manufacturer: " + this.manufacturer + ", " +
                "Number of Gears: " + this.numberGears + ", " +
                "Max Speed: " + this.maxSpeed;
    }
}